package sample;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.awt.*;

public class Box implements GameObject {
    private int hitPoints;
    private double xCord;
    private double yCord;
    private Rectangle box;
    private Label counter;
    private Pane holder;
    private Pane screen;

    /**
     * Constrctor
     * @param maxHitPoints  Our Roof Value
     * @param screen To place box on
     * @param x coordinate
     * @param y coordinate
     */
    public Box(int maxHitPoints, Pane screen, int x, int y){
        hitPoints = (int)(Math.random() * maxHitPoints);
        box = new Rectangle(120, 120, determineColor());
        //This makes rounded corners on rectangles
        //box.setArcWidth(30.0);
        //box.setArcHeight(20.0);
        box.setStroke(Color.GREY);
        counter = new Label(Integer.toString(hitPoints));
        counter.setFont(Font.font("Calbri", 75));
        counter.setLayoutX(15);
        holder = new Pane();
        holder.getChildren().add(box);
        holder.getChildren().add(counter);
        screen.getChildren().add(holder);
        xCord= x;
        yCord = y;
        holder.setLayoutX(xCord);
        holder.setLayoutY(yCord);

    }

    /**
     * Determines color of box based on hitpoints
     * TO-DO: Turn into enumeration
     * @return color
     */
    private Color determineColor(){
        if (hitPoints >= 50){
            return  Color.web("#f45642");
        }
        else if (hitPoints >=40){
            return  Color.web("#ed8f3d");
        }
        else if (hitPoints >=30){
            return  Color.web("#f2e63c");
        }
        else if (hitPoints >=20){
            return  Color.web("#8ce837");
        }
        else if (hitPoints >=10){
            return  Color.web("#33ddbe");
        }
        else{
            return Color.web("#59baea");
        }
    }


}
