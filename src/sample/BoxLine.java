package sample;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class BoxLine {

    private final int AMOUNT_OF_BOXES = 6;
    private int yCord;
    ArrayList<Box> line = new ArrayList<>();

    public BoxLine(int maxHitPoints, Pane screen, int yCord){
        int x = 5;
        this.yCord = yCord;
        for (int i = 0; i<AMOUNT_OF_BOXES; i++){
            line.add(new Box(maxHitPoints, screen, x, yCord));
            x = x + 120;
        }

    }
}
