package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Pane screen;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        screen.setStyle("-fx-background-color: #363738;");
        BoxLine boxLine = new BoxLine(50, screen, 500);
    }
}
